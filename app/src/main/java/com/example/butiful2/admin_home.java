package com.example.butiful2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class admin_home extends AppCompatActivity  implements   View.OnClickListener {


    String name, name2, name3;
    ImageView btnimg1 , btnimg2, btnimg3, btnimg4, btnimg5, btnimg6, btnimg7;
    ImageView imgnext1 , imgnext2, imgnext3, imgnext4, imgnext5, imgnext6, imgnext7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);


        Bundle b = getIntent().getExtras();
        name = b.getString("A");

        btnimg1 = (ImageView)findViewById(R.id.ic1);
        btnimg2 = (ImageView)findViewById(R.id.ic2);
        btnimg3 = (ImageView)findViewById(R.id.ic3);

        btnimg1.setOnClickListener(this);
        btnimg2.setOnClickListener(this);
        btnimg3.setOnClickListener(this);


        imgnext1 = (ImageView)findViewById(R.id.imgnext);
        imgnext2 = (ImageView)findViewById(R.id.imgnext2);
        imgnext3 = (ImageView)findViewById(R.id.imgnext3);
        imgnext4 = (ImageView)findViewById(R.id.imgnext4);
        imgnext5 = (ImageView)findViewById(R.id.imgnext5);
        imgnext6 = (ImageView)findViewById(R.id.imgnext6);
        imgnext7 = (ImageView)findViewById(R.id.imgnext7);

        imgnext1.setOnClickListener(this);
        imgnext2.setOnClickListener(this);
        imgnext3.setOnClickListener(this);
        imgnext4.setOnClickListener(this);
        imgnext5.setOnClickListener(this);
        imgnext6.setOnClickListener(this);
        imgnext7.setOnClickListener(this);


    }


    public void onClick(View view) {
        if(view == btnimg1) {

            Intent intent = new Intent(admin_home.this, admin_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == btnimg2) {

            Intent intent = new Intent(admin_home.this, product.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == btnimg3) {

            Intent intent = new Intent(admin_home.this, qdata.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == imgnext1) {

            Intent intent = new Intent(admin_home.this, qdata.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == imgnext2) {
            Intent intent = new Intent(admin_home.this, qdata2.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == imgnext3) {
            Intent intent = new Intent(admin_home.this, product.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == imgnext4) {
            Intent intent = new Intent(admin_home.this, staff.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == imgnext5) {
            Intent intent = new Intent(admin_home.this, member.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == imgnext6) {
            Intent intent = new Intent(admin_home.this, report.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == imgnext7) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Confirm");
            builder.setMessage(" กรุณายืนยันการออกจากระบบ ?");

            builder.setPositiveButton(" ออกจากระบบ ", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            builder.setNegativeButton(" ยกเลิก ", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();


        }
    }

    public void onBackPressed() {

    }


}