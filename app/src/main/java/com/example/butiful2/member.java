package com.example.butiful2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class member extends AppCompatActivity implements   View.OnClickListener {

    private Context context;
    String name, name2, name3;
    String d1 = "";
    String d2 = "";
    private TextView bm1, bm2, bm3, bm4, bm5, bm6 ;
    private EditText no1, no2, no3, no4, no5, no6, no7, no8;
    String Str1,Str2,Str3,Str4,Str5,Str6,Str7,nameimg,Str8;
    private Button btnConfrim;
    private Button btnBack;
    private Button btnView;
    private Button btnall;
    ImageView btnimg1 , btnimg2, btnimg3, btnimg4, btnimg5;
    int sumall = 0;
    int sumalltotalpoint = 0;
    int cart_count=0;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private int date, month, yeat;
    String formattedDate2, formattedDate3;
    private static final String readdata3 = "https://project-alltest.com/appbutiful2/load_member2.php";
    private static final String UPLOAD_URL3 = "https://project-alltest.com/appbutiful2/deletemember2.php";
    ImageView btnback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member);

        Bundle b = getIntent().getExtras();
        name = b.getString("A");


        btnimg1 = (ImageView)findViewById(R.id.ic1);
        btnimg2 = (ImageView)findViewById(R.id.ic2);
        btnimg3= (ImageView)findViewById(R.id.ic3);

        btnimg1.setOnClickListener(this);
        btnimg2.setOnClickListener(this);
        btnimg3.setOnClickListener(this);



        String urlSuffix3 = "?member_user=";
        class RegisterUser3 extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(member.this, "Please Wait", null, true, true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                int A = s.length();

                readdatalist(s);

            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(readdata3 + s);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String result;

                    result = bufferedReader.readLine();

                    return result;
                } catch (Exception e) {
                    return null;
                }
            }
        }

        RegisterUser3 ru3 = new RegisterUser3();
        ru3.execute(urlSuffix3);

        no1 = (EditText) findViewById(R.id.e1);
        no1.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                String gettext = no1.getText().toString();
                String urlSuffix3 = "?member_user="+gettext;
                class RegisterUser3 extends AsyncTask<String, Void, String> {

                    ProgressDialog loading;

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        loading = ProgressDialog.show(member.this, "Please Wait", null, true, true);

                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        loading.dismiss();
                        int A = s.length();

                        readdatalist(s);

                    }

                    @Override
                    protected String doInBackground(String... params) {
                        String s = params[0];
                        BufferedReader bufferedReader = null;
                        try {
                            URL url = new URL(readdata3 + s);
                            HttpURLConnection con = (HttpURLConnection) url.openConnection();
                            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                            String result;

                            result = bufferedReader.readLine();

                            return result;
                        } catch (Exception e) {
                            return null;
                        }
                    }
                }

                RegisterUser3 ru3 = new RegisterUser3();
                ru3.execute(urlSuffix3);

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        btnall = (Button) findViewById(R.id.btnall);
        btnall.setOnClickListener(this);


        btnback = (ImageView) findViewById(R.id.imgbtnback);
        btnback.setOnClickListener(this);
    }

    private void readdatalist(String s) {
        String strJSON = s;
        try {
            JSONArray data = new JSONArray(strJSON);

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);
                int Row = i + 1;
                map = new HashMap<String, String>();
                map.put("count", Integer.toString(Row));
                map.put("id", c.getString("id"));
                map.put("name", c.getString("name"));
                map.put("telphone", c.getString("telphone"));
                map.put("address", c.getString("address"));
                map.put("username", c.getString("username"));
                map.put("password", c.getString("password"));
                map.put("status", c.getString("status"));
                map.put("img", c.getString("img"));

                MyArrList.add(map);

            }


            final ListView listViewMovies = (ListView) findViewById(R.id.list1);
            listViewMovies.setAdapter(new ImageAdapter(this, MyArrList));

            final androidx.appcompat.app.AlertDialog.Builder viewDetail = new androidx.appcompat.app.AlertDialog.Builder(this);
            final LayoutInflater factory = LayoutInflater.from(this);
            // OnClick Item
            listViewMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView,
                                        int position, long mylng) {


                }
            });

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public class ImageAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter(Context c, ArrayList<HashMap<String, String>> list) {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.activity_column_member, null);
            }



            TextView txtPosition1 = (TextView) convertView.findViewById(R.id.name_order);
            txtPosition1.setText(Html.fromHtml(  MyArr.get(position).get("name") ));


            // ColPosition
            ImageView btnedit = (ImageView) convertView.findViewById(R.id.btnedite);
            btnedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    final String id_order = MyArr.get(position).get("username");
                    final String data1 = MyArr.get(position).get("name");
                    final String data2 = MyArr.get(position).get("telphone");
                    final String data3 = MyArr.get(position).get("address");
                    final String data4 = MyArr.get(position).get("username");
                    final String data5 = MyArr.get(position).get("password");
                    final String data6 = MyArr.get(position).get("status");
                    final String data7 = MyArr.get(position).get("img");


                    Intent intent = new Intent(member.this, member_edit.class);
                    intent.putExtra("A", name);
                    intent.putExtra("D1", id_order);
                    intent.putExtra("D2", data1);
                    intent.putExtra("D3", data2);
                    intent.putExtra("D4", data3);
                    intent.putExtra("D5", data4);
                    intent.putExtra("D6", data5);
                    intent.putExtra("D7", data6);
                    intent.putExtra("D8", data7);
                    startActivity(intent);
                    finish();
                }
            });

            ImageView btnedit2 = (ImageView) convertView.findViewById(R.id.btnview);
            btnedit2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    final String id_order = MyArr.get(position).get("username");
                    final String data1 = MyArr.get(position).get("name");
                    final String data2 = MyArr.get(position).get("telphone");
                    final String data3 = MyArr.get(position).get("address");
                    final String data4 = MyArr.get(position).get("username");
                    final String data5 = MyArr.get(position).get("password");
                    final String data6 = MyArr.get(position).get("status");
                    final String data7 = MyArr.get(position).get("img");


                    Intent intent = new Intent(member.this, member_edit.class);
                    intent.putExtra("A", name);
                    intent.putExtra("D1", id_order);
                    intent.putExtra("D2", data1);
                    intent.putExtra("D3", data2);
                    intent.putExtra("D4", data3);
                    intent.putExtra("D5", data4);
                    intent.putExtra("D6", data5);
                    intent.putExtra("D7", data6);
                    intent.putExtra("D8", data7);
                    startActivity(intent);
                    finish();
                }
            });


            ImageView btndelete = (ImageView) convertView.findViewById(R.id.btndelete);
            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final String id_order = MyArr.get(position).get("id");

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member.this);
                    builder.setCancelable(false);
                    builder.setTitle(" ยืนยันการลบรายการ ");
                    builder.setMessage(Html.fromHtml(" ยืนยันการลบรายการ :   " + MyArr.get(position).get("name")));
                    builder.setNegativeButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                            String urlSuffix = "?id_delete=" + id_order;
                            class RegisterUser extends AsyncTask<String, Void, String> {

                                ProgressDialog loading;

                                @Override
                                protected void onPreExecute() {
                                    super.onPreExecute();
                                    loading = ProgressDialog.show(member.this, "Please Wait", null, true, true);
                                }

                                @Override
                                protected void onPostExecute(String s) {
                                    super.onPostExecute(s);
                                    loading.dismiss();
                                    int A = s.length();
                                    if (s.equals("บันทึกไม่สำเร็จ")) {

                                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member.this);
                                        builder.setCancelable(false);
                                        builder.setTitle("แจ้งเตือน");
                                        builder.setMessage(Html.fromHtml(" ทำรายการไม่สำเร็จ "));
                                        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();


                                            }
                                        });
                                        builder.show();
                                    } else if (s.equals("บันทึกเรียบร้อย")) {


                                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member.this);
                                        builder.setCancelable(false);
                                        builder.setTitle("แจ้งเตือน");
                                        builder.setMessage(Html.fromHtml(" ลบรายการเรียบร้อย "));
                                        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();



                                            }
                                        });
                                        builder.show();

                                    }
                                }

                                @Override
                                protected String doInBackground(String... params) {
                                    String s = params[0];
                                    BufferedReader bufferedReader = null;
                                    try {
                                        URL url = new URL(UPLOAD_URL3 + s);
                                        HttpURLConnection con = (HttpURLConnection) url.openConnection();
                                        bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                                        String result;

                                        result = bufferedReader.readLine();

                                        return result;
                                    } catch (Exception e) {
                                        return null;
                                    }
                                }
                            }
                            RegisterUser ru = new RegisterUser();
                            ru.execute(urlSuffix);


                            String urlSuffix3 = "?member_user=";
                            class RegisterUser3 extends AsyncTask<String, Void, String> {

                                ProgressDialog loading;

                                @Override
                                protected void onPreExecute() {
                                    super.onPreExecute();
                                    loading = ProgressDialog.show(member.this, "Please Wait", null, true, true);

                                }

                                @Override
                                protected void onPostExecute(String s) {
                                    super.onPostExecute(s);
                                    loading.dismiss();
                                    int A = s.length();

                                    readdatalist(s);

                                }

                                @Override
                                protected String doInBackground(String... params) {
                                    String s = params[0];
                                    BufferedReader bufferedReader = null;
                                    try {
                                        URL url = new URL(readdata3 + s);
                                        HttpURLConnection con = (HttpURLConnection) url.openConnection();
                                        bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                                        String result;

                                        result = bufferedReader.readLine();

                                        return result;
                                    } catch (Exception e) {
                                        return null;
                                    }
                                }
                            }

                            RegisterUser3 ru3 = new RegisterUser3();
                            ru3.execute(urlSuffix3);

                        }
                    });

                    builder.setPositiveButton("ไม่", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();
                }
            });

            return convertView;

        }


    }

    public void onClick(View view) {
        if(view == btnback) {

            Intent intent = new Intent(member.this, admin_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if(view == btnall) {

            String urlSuffix3 = "?member_user=";
            class RegisterUser3 extends AsyncTask<String, Void, String> {

                ProgressDialog loading;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    loading = ProgressDialog.show(member.this, "Please Wait", null, true, true);

                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    loading.dismiss();
                    int A = s.length();

                    readdatalist(s);

                }

                @Override
                protected String doInBackground(String... params) {
                    String s = params[0];
                    BufferedReader bufferedReader = null;
                    try {
                        URL url = new URL(readdata3 + s);
                        HttpURLConnection con = (HttpURLConnection) url.openConnection();
                        bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                        String result;

                        result = bufferedReader.readLine();

                        return result;
                    } catch (Exception e) {
                        return null;
                    }
                }
            }

            RegisterUser3 ru3 = new RegisterUser3();
            ru3.execute(urlSuffix3);


        }else  if(view == btnimg1) {

            Intent intent = new Intent(member.this, admin_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == btnimg2) {

            Intent intent = new Intent(member.this, product.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if(view == btnimg3) {

            Intent intent = new Intent(member.this, qdata.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();


        }
    }

    public void onBackPressed() {

    }
}