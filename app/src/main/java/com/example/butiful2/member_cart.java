package com.example.butiful2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class member_cart extends AppCompatActivity  implements   View.OnClickListener {

    private Context context;
    String name, name2, name3;
    String d1 = "";
    String d2 = "";
    private TextView bm1, bm2, bm3, bm4, bm5, bm6 ;
    private EditText no1, no2, no3, no4, no5, no6, no7, no8;
    String Str1,Str2,Str3,Str4,Str5,Str6,Str7,nameimg,Str8;
    private static final String readdata3 = "https://project-alltest.com/appbutiful2/load_cart.php"; /// สินค้า
    private static final String UPLOAD_URL = "https://project-alltest.com/appbutiful2/deletecart.php"; /// ลบสินค้า
    private static final String UPLOAD_URL2 = "https://project-alltest.com/appbutiful2/saveorder_update.php"; // อัพเดตสินค้า

    private static final String Save_confrim = "https://project-alltest.com/appbutiful2/saveconfrimorder.php"; ///
    private Button btnConfrim;
    private Button btnBack;
    private Button btnView;
    ImageView btnimg1 , btnimg2, btnimg3, btnimg4, btnimg5;
    int sumall = 0;
    int sumalltotalpoint = 0;
    int cart_count=0;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private int date, month, yeat;
    String formattedDate2, formattedDate3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_cart);


        Bundle b = getIntent().getExtras();
        name = b.getString("A");

        btnimg1 = (ImageView)findViewById(R.id.ic1);
        btnimg2 = (ImageView)findViewById(R.id.ic2);
        btnimg3= (ImageView)findViewById(R.id.ic3);

        btnimg1.setOnClickListener(this);
        btnimg2.setOnClickListener(this);
        btnimg3.setOnClickListener(this);


        bm1 = (TextView) findViewById(R.id.totalprice);
        String urlSuffix3 =  "?txtKeyword="+name;
        class RegisterUser3 extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(member_cart.this, "Please Wait", null, true, true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                int A = s.length();

                readdatalist(s);

            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(readdata3 + s);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String result;

                    result = bufferedReader.readLine();

                    return result;
                } catch (Exception e) {
                    return null;
                }
            }
        }

        RegisterUser3 ru3 = new RegisterUser3();
        ru3.execute(urlSuffix3);

        loadpricetotal();

        btnConfrim = (Button) findViewById(R.id.savedata);
        btnConfrim.setOnClickListener(this);

        no1 = (EditText) findViewById(R.id.e1);
        no2 = (EditText) findViewById(R.id.e2);
        no3 = (EditText) findViewById(R.id.e3);
        no1.setOnClickListener(this);
        no2.setOnClickListener(this);

        Calendar c2 = Calendar.getInstance();
        SimpleDateFormat df2 = new SimpleDateFormat("HH:mm");
        formattedDate2 = df2.format(c2.getTime());
        SimpleDateFormat df3 = new SimpleDateFormat("dd-MM-yyyy");
        formattedDate3 = df3.format(c2.getTime());
        date = c2.get(Calendar.DAY_OF_MONTH);
        month = c2.get(Calendar.MONTH);
        yeat = c2.get(Calendar.YEAR);


        btnBack = (Button) findViewById(R.id.addproduct);
        btnBack.setOnClickListener(this);


        btnView = (Button) findViewById(R.id.btnviewcalendar);
        btnView.setOnClickListener(this);

    }


    private void loadpricetotal() {
        String urlSuffix3 =  "?txtKeyword="+name;
        class RegisterUser3 extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(member_cart.this, "Please Wait", null, true, true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                int A = s.length();

                readdatalist2(s);

            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(readdata3 + s);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String result;

                    result = bufferedReader.readLine();

                    return result;
                } catch (Exception e) {
                    return null;
                }
            }
        }

        RegisterUser3 ru3 = new RegisterUser3();
        ru3.execute(urlSuffix3);
    }

    private void readdatalist(String s) {
        sumall = 0;
        sumalltotalpoint = 0;

        String strJSON = s;
        try {
            JSONArray data = new JSONArray(strJSON);

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);
                int Row = i + 1;
                map = new HashMap<String, String>();
                map.put("count", Integer.toString(Row));
                map.put("id", c.getString("id"));
                map.put("name", c.getString("name"));
                map.put("price", c.getString("price"));
                map.put("price2", c.getString("price2"));
                map.put("img", c.getString("img"));
                map.put("total", c.getString("total"));
                map.put("bill_no", c.getString("bill_no"));
                map.put("date_start", c.getString("date_start"));
                map.put("date_time", c.getString("date_time"));
                map.put("status_payment", c.getString("status_payment"));


                String totalpro = c.getString("total");
                String priceall = c.getString("price");
                sumall += (Integer.parseInt(totalpro) * Integer.parseInt(priceall));
                sumalltotalpoint += (Integer.parseInt(totalpro));


                MyArrList.add(map);

            }
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String yourFormattedString = formatter.format(sumall);
            bm1.setText(" ยอดเงินชำระ : " + yourFormattedString + " บาท ");


            final ListView listViewMovies = (ListView) findViewById(R.id.list1);
            listViewMovies.setAdapter(new ImageAdapter(this, MyArrList));

            final androidx.appcompat.app.AlertDialog.Builder viewDetail = new androidx.appcompat.app.AlertDialog.Builder(this);
            final LayoutInflater factory = LayoutInflater.from(this);
            // OnClick Item
            listViewMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView,
                                        int position, long mylng) {


                }
            });



        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void readdatalist2(String s) {
        sumall = 0;
        sumalltotalpoint = 0;
        String strJSON = s;
        try {
            JSONArray data = new JSONArray(strJSON);

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);
                int Row = i + 1;
                map = new HashMap<String, String>();
                map.put("count", Integer.toString(Row));
                map.put("id", c.getString("id"));
                map.put("name", c.getString("name"));
                map.put("price", c.getString("price"));
                map.put("price2", c.getString("price2"));
                map.put("img", c.getString("img"));
                map.put("total", c.getString("total"));
                map.put("bill_no", c.getString("bill_no"));
                map.put("date_start", c.getString("date_start"));
                map.put("date_time", c.getString("date_time"));
                map.put("status_payment", c.getString("status_payment"));


                String totalpro = c.getString("total");
                String priceall = c.getString("price");
                sumall += (Integer.parseInt(totalpro) * Integer.parseInt(priceall));
                sumalltotalpoint += (Integer.parseInt(totalpro));


                MyArrList.add(map);

            }

            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String yourFormattedString = formatter.format(sumall);
            bm1.setText(" ยอดเงินชำระ : " + yourFormattedString + " บาท ");




        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public class ImageAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter(Context c, ArrayList<HashMap<String, String>> list) {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.activity_column_cart, null);
            }


            ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView2);
            String chk1 = MyArr.get(position).get("img");
            String url = "";
            if (!MyArr.get(position).get("img").equals("")) {

                url = "https://project-alltest.com/appbutiful2/img/" + MyArr.get(position).get("img");
                Picasso.get().load(url).into(imageView);

            } else {

            }

            // ColPosition
            TextView txtPosition1 = (TextView) convertView.findViewById(R.id.name_order);
            txtPosition1.setText(Html.fromHtml(MyArr.get(position).get("name") ));

            TextView txtPosition2 = (TextView) convertView.findViewById(R.id.name_order2);
            txtPosition2.setText(Html.fromHtml( " ราคา  " + MyArr.get(position).get("price2") +  " บาท " ));

            final TextView quantityText = (TextView) convertView.findViewById(R.id.quantity);
            quantityText.setText(" จำนวน  " + MyArr.get(position).get("total").toString() + " รายการ ");



            // ColPosition
            ImageView btndelete = (ImageView) convertView.findViewById(R.id.btndelete);
            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final String id_order = MyArr.get(position).get("id");

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_cart.this);
                    builder.setCancelable(false);
                    builder.setTitle(" ยืนยันการลบรายการ ");
                    builder.setMessage(Html.fromHtml(" ยืนยันการลบรายการ :   " + MyArr.get(position).get("name")));
                    builder.setNegativeButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                            String urlSuffix = "?id_delete=" + id_order;

                            class RegisterUser extends AsyncTask<String, Void, String> {

                                ProgressDialog loading;

                                @Override
                                protected void onPreExecute() {
                                    super.onPreExecute();
                                    loading = ProgressDialog.show(member_cart.this, "Please Wait", null, true, true);
                                }

                                @Override
                                protected void onPostExecute(String s) {
                                    super.onPostExecute(s);
                                    loading.dismiss();
                                    int A = s.length();
                                    if (s.equals("บันทึกไม่สำเร็จ")) {

                                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_cart.this);
                                        builder.setCancelable(false);
                                        builder.setTitle("แจ้งเตือน");
                                        builder.setMessage(Html.fromHtml(" ทำรายการไม่สำเร็จ "));
                                        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();


                                            }
                                        });
                                        builder.show();
                                    } else if (s.equals("บันทึกเรียบร้อย")) {


                                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_cart.this);
                                        builder.setCancelable(false);
                                        builder.setTitle("แจ้งเตือน");
                                        builder.setMessage(Html.fromHtml(" ลบรายการเรียบร้อย "));
                                        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();

                                                Intent intent = new Intent(member_cart.this, member_cart.class);
                                                intent.putExtra("A", name);
                                                startActivity(intent);
                                                finish();

                                            }
                                        });
                                        builder.show();

                                    }
                                }

                                @Override
                                protected String doInBackground(String... params) {
                                    String s = params[0];
                                    BufferedReader bufferedReader = null;
                                    try {
                                        URL url = new URL(UPLOAD_URL + s);
                                        HttpURLConnection con = (HttpURLConnection) url.openConnection();
                                        bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                                        String result;

                                        result = bufferedReader.readLine();

                                        return result;
                                    } catch (Exception e) {
                                        return null;
                                    }
                                }
                            }

                            RegisterUser ru = new RegisterUser();
                            ru.execute(urlSuffix);


                            loadpricetotal();

                        }
                    });

                    builder.setPositiveButton("ไม่", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();

                }
            });


            return convertView;

        }


    }


    public void onClick(View view) {
        if(view == btnimg1) {

            Intent intent = new Intent(member_cart.this, member_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == btnimg2) {

            Intent intent = new Intent(member_cart.this, member_cart.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if(view == btnimg3) {

            Intent intent = new Intent(member_cart.this, member_history.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if(view == btnBack) {

            Intent intent = new Intent(member_cart.this, member_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if(view == btnView) {

            AlertDialog.Builder builder = new AlertDialog.Builder(member_cart.this);
            LayoutInflater inflater = getLayoutInflater();

            View view2 = inflater.inflate(R.layout.alert_map,null);

            WebView myWebView = (WebView) view2.findViewById(R.id.myWebView);
            WebSettings webSettings = myWebView.getSettings();
            webSettings.setDomStorageEnabled(true);
            webSettings.setLoadWithOverviewMode(true);
            webSettings.setUseWideViewPort(true);
            webSettings.setBuiltInZoomControls(true);
            webSettings.setDisplayZoomControls(false);
            webSettings.setSupportZoom(true);
            webSettings.setDefaultTextEncodingName("utf-8");
            webSettings.setJavaScriptEnabled(true);
            myWebView.setWebChromeClient(new WebChromeClient());
            myWebView.setWebViewClient(new WebViewClient());
            myWebView.loadUrl("https://project-alltest.com/appbutiful2/calendar/viewcalendar.php?car=");


            builder.setView(view2);
            builder.show();


        }else if(view == no1) {

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    no1.setText(i2 + "-" + (i1 + 1) + "-" + i);

                }
            }
                    , yeat, month, date);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();

        } else if (view == no2) {

            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            String ti1 = "";
                            if(hourOfDay <= 9){
                                ti1 = "0"+hourOfDay;
                            }else{
                                ti1 = Integer.toString(hourOfDay);
                            }
                            String ti2 = "";
                            if(minute <= 9){
                                ti2 = "0"+minute;
                            }else{
                                ti2 = Integer.toString(minute);
                            }

                            no2.setText(ti1 + ":" + ti2);
                        }
                    }, mHour, mMinute, true);
            timePickerDialog.show();

        }else  if (view == btnConfrim) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("  คำตือน ");
            builder.setMessage(" กรุณาตรวจสอบรายการ \n ก่อนกดยืนยันถ้ากดยืนยันไปแล้วจะไม่ \n" + " สามารถทำการแก้ไขรายการได้อีก ?");

            builder.setPositiveButton(" ตกลง ", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                    Str1 = no1.getText().toString().trim();
                    Str2 = no2.getText().toString().trim();
                    Str3 = no3.getText().toString().trim();

                    if (Str1.equals("")) {

                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_cart.this);
                        builder.setCancelable(false);
                        builder.setTitle("แจ้งเตือน");
                        builder.setMessage(" กรุณา ระบุวันที่ ");
                        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        builder.show();

                    }else if (Str2.equals("")) {

                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_cart.this);
                        builder.setCancelable(false);
                        builder.setTitle("แจ้งเตือน");
                        builder.setMessage(" กรุณา ระบุเวลา ");
                        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        builder.show();
                    }else{

                        String urlSuffix = "?member_user=" + name + "&data1="+Str1+ "&data2="+Str2+ "&data3="+Str3;
                        class RegisterUser extends AsyncTask<String, Void, String> {

                            ProgressDialog loading;

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                loading = ProgressDialog.show(member_cart.this, "Please Wait", null, true, true);
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                super.onPostExecute(s);
                                loading.dismiss();
                                int A = s.length();
                                if (s.equals("บันทึกไม่สำเร็จ")) {

                                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_cart.this);
                                    builder.setCancelable(false);
                                    builder.setTitle("แจ้งเตือน");
                                    builder.setMessage(Html.fromHtml(" ทำรายการไม่สำเร็จ "));
                                    builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();


                                        }
                                    });
                                    builder.show();
                                } else if (s.equals("บันทึกเรียบร้อย")) {


                                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_cart.this);
                                    builder.setCancelable(false);
                                    builder.setTitle("บันทึกเรียบร้อย");
                                    builder.setMessage(Html.fromHtml(" โปรดติดตามสถานะการจองคิว "));
                                    builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();

                                            Intent intent = new Intent(member_cart.this, member_history.class);
                                            intent.putExtra("A", name);
                                            startActivity(intent);
                                            finish();

                                        }
                                    });
                                    builder.show();

                                }
                            }

                            @Override
                            protected String doInBackground(String... params) {
                                String s = params[0];
                                BufferedReader bufferedReader = null;
                                try {
                                    URL url = new URL(Save_confrim + s);
                                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                                    String result;

                                    result = bufferedReader.readLine();

                                    return result;
                                } catch (Exception e) {
                                    return null;
                                }
                            }
                        }
                        RegisterUser ru = new RegisterUser();
                        ru.execute(urlSuffix);

                    }

                }
            });

            builder.setNegativeButton(" ไม่ตกลง ", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();

        }
    }


    public void onBackPressed() {

    }
}