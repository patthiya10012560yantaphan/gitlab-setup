package com.example.butiful2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.denzcoskun.imageslider.transformation.RoundedTransformation;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class member_home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,  View.OnClickListener {

    private Context context;
    String name, name2, name3;
    String d1 = "";
    String d2 = "";
    private TextView bm1, bm2, bm3, bm4, bm5, bm6 ;

    private EditText no1, no2, no3, no4, no5, no6, no7, no8;
    String Str1,Str2,Str3,Str4,Str5,Str6,Str7,nameimg,Str8;
    private static final String readdata1 = "https://project-alltest.com/appbutiful2/readdata1.php"; /// โหลดชื่อผู้ใช้
    private static final String readdata2 = "https://project-alltest.com/appbutiful2/load_noti_total.php"; /// ตะกร้า

    int cart_count=0;
    String typepro = "";


    ImageView btnimg1 , btnimg2, btnimg3, btnimg4, btnimg5;
    private static final String Loadproduct = "https://project-alltest.com/appbutiful2/loadproduct.php";
    String roomName;
    LinearLayout.LayoutParams layoutParam;
    private static final String UPLOAD_URL = "https://project-alltest.com/appbutiful2/saveorder.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headView = navigationView.getHeaderView(0);
        final TextView txtuser = headView.findViewById(R.id.username);


        Bundle b = getIntent().getExtras();
        name = b.getString("A");


        String urlSuffix = "?txtKeyword="+name;
        class RegisterUser extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(member_home.this, "Please Wait",null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                int A = s.length();

                String strJSON = s;
                try {
                    JSONArray data = new JSONArray(strJSON);

                    final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
                    HashMap<String, String> map;

                    for(int i = 0; i < data.length(); i++){
                        JSONObject c = data.getJSONObject(i);
                        int Row = i+1;
                        map = new HashMap<String, String>();
                        d1 =  c.getString("name");
                        MyArrList.add(map);

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                txtuser.setText(Html.fromHtml("  <font color = 'White' > " + d1 + " </font>   "));
            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(readdata1+s);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String result;

                    result = bufferedReader.readLine();

                    return result;
                }catch(Exception e){
                    return null;
                }
            }
        }

        RegisterUser ru = new RegisterUser();
        ru.execute(urlSuffix);

        ///////////////////////////////////////////////////////////

        btnimg1 = (ImageView)findViewById(R.id.ic1);
        btnimg2 = (ImageView)findViewById(R.id.ic2);
        btnimg3= (ImageView)findViewById(R.id.ic3);

        btnimg1.setOnClickListener(this);
        btnimg2.setOnClickListener(this);
        btnimg3.setOnClickListener(this);



        String urlSuffix3 = "?txtKeyword="+"&typepro=";
        class RegisterUser3 extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                int A = s.length();

                readdatalist2(s);

            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(Loadproduct + s);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String result;

                    result = bufferedReader.readLine();

                    return result;
                } catch (Exception e) {
                    return null;
                }
            }
        }

        RegisterUser3 ru3 = new RegisterUser3();
        ru3.execute(urlSuffix3);
    }

    private void readdatalist2(String s) {

        String strJSON = s;
        try {
            JSONArray data = new JSONArray(strJSON);

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);
                int Row = i + 1;
                map = new HashMap<String, String>();
                map.put("count", Integer.toString(Row));
                map.put("id", c.getString("id"));
                map.put("img", c.getString("img"));
                map.put("name", c.getString("name"));
                map.put("price1", c.getString("price1"));
                MyArrList.add(map);

            }

            final GridView gView1 = (GridView) findViewById(R.id.gridView1);
            gView1.setAdapter(new ImageAdapter(this, MyArrList));


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public class ImageAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter(Context c, ArrayList<HashMap<String, String>> list) {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.activity_column_pro, null);
            }

            ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView2);
            String chk1 = MyArr.get(position).get("img");
            String url = "";
            if (!MyArr.get(position).get("img").equals("")) {

                url = "https://project-alltest.com/appbutiful2/img/" + MyArr.get(position).get("img");
                Picasso.get().load(url).resize(400, 350).transform(new RoundedTransformation(50, 0)).into(imageView);

            }

            // ColPosition
            TextView txtPosition1 = (TextView) convertView.findViewById(R.id.data1);
            txtPosition1.setText(Html.fromHtml(MyArr.get(position).get("name")));

            // ColPosition
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            int newsmoney = Integer.parseInt(MyArr.get(position).get("price1"));
            String yourFormattedString = formatter.format(newsmoney);

            TextView txtPosition2 = (TextView) convertView.findViewById(R.id.data2);
            txtPosition2.setText(Html.fromHtml( "  ราคา : " + yourFormattedString + " บาท " ));


            TextView btnedit = (TextView) convertView.findViewById(R.id.btnsave);
            btnedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final String id_order = MyArr.get(position).get("id");
                    final String data1 = MyArr.get(position).get("name");
                    final String data3 = MyArr.get(position).get("price1");
                    final String data5 = MyArr.get(position).get("img");



                    String urlSuffix = "?menu_id="+id_order+ "&price="+data3+ "&total=1"+"&member_user="+name;
                    class RegisterUser extends AsyncTask<String, Void, String> {

                        ProgressDialog loading;

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            int A = s.length();
                            if(s.equals("บันทึกไม่สำเร็จ")){

                                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_home.this);
                                builder.setCancelable(false);
                                builder.setTitle("แจ้งเตือน");
                                builder.setMessage(" บันทึกไม่สำเร็จ ");
                                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                builder.show();

                            }else if(s.equals("บันทึกเรียบร้อย")){

                                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_home.this);
                                builder.setCancelable(false);
                                builder.setTitle("แจ้งเตือน");
                                builder.setMessage(" เพิ่มรายการสินค้าลงตะกร้า เรียบร้อย ");
                                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();

                                        Intent intent = new Intent(member_home.this, member_home.class);
                                        intent.putExtra("A", name);
                                        startActivity(intent);
                                        finish();

                                    }
                                });
                                builder.show();

                            }
                        }

                        @Override
                        protected String doInBackground(String... params) {
                            String s = params[0];
                            BufferedReader bufferedReader = null;
                            try {
                                URL url = new URL(UPLOAD_URL+s);
                                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                                bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                                String result;

                                result = bufferedReader.readLine();

                                return result;
                            }catch(Exception e){
                                return null;
                            }
                        }
                    }
                    RegisterUser ru = new RegisterUser();
                    ru.execute(urlSuffix);


                }
            });




            return convertView;

        }


    }

    public void onClick(View view) {
        if(view == btnimg1) {

            Intent intent = new Intent(member_home.this, member_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == btnimg2) {

            Intent intent = new Intent(member_home.this, member_cart.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if(view == btnimg3) {

            Intent intent = new Intent(member_home.this, member_history.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.member_home, menu);

        final MenuItem menuItem = menu.findItem(R.id.cart_action);
        String urlSuffix2 = "?txtKeyword="+name;
        class RegisterUser2 extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(member_home.this, "Please Wait",null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                int A = s.length();

                String strJSON = s;
                try {
                    JSONArray data = new JSONArray(strJSON);

                    final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
                    HashMap<String, String> map;

                    for(int i = 0; i < data.length(); i++){
                        JSONObject c = data.getJSONObject(i);
                        int Row = i+1;
                        map = new HashMap<String, String>();
                        d2 =  c.getString("cart");
                        MyArrList.add(map);

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                cart_count = Integer.parseInt(d2);

                menuItem.setIcon(Converter.convertLayoutToImage(member_home.this,cart_count,R.drawable.cart));
                menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        Intent intent = new Intent(member_home.this, member_cart.class);
                        intent.putExtra("A", name);
                        startActivity(intent);
                        finish();

                        return true;
                    }
                });

            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(readdata2+s);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String result;

                    result = bufferedReader.readLine();

                    return result;
                }catch(Exception e){
                    return null;
                }
            }
        }

        RegisterUser2 ru2 = new RegisterUser2();
        ru2.execute(urlSuffix2);


        return true;
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu1) {

            Intent intent = new Intent(member_home.this, member_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        } else if (id == R.id.menu2) {

            Intent intent = new Intent(member_home.this, member_profile.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        } else if (id == R.id.menuout) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Confirm");
            builder.setMessage(" กรุณายืนยันการออกจากระบบ ?");

            builder.setPositiveButton(" ออกจากระบบ ", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(member_home.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

            builder.setNegativeButton(" ไม่ออก ", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onBackPressed() {

    }


}