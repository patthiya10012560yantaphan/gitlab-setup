package com.example.butiful2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

public class member_profile extends AppCompatActivity  implements View.OnClickListener{


    String name, name2, name3 ;
    String d1 = "";
    String d2 = "";
    String d3 = "";
    String d4 = "";
    String d5 = "";
    String d6 = "";
    String d7 = "";
    String d8 = "";
    String d9 = "";
    String d10 = "";
    String d11 = "";
    String d12 = "";
    String d13 = "";

    private static final int IMAGE_REQUEST_CODE = 3;
    private static final int STORAGE_PERMISSION_CODE = 123;
    private ImageView bug2_image ;
    private Bitmap bitmap;
    private Uri filePath;
    private TextView tvPath;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    String image_camera = "0";
    String path_camera = "";
    private Button buttonsave, buttoncancel;
    String formattedDate2;

    TextView bynuploadimg;


    String getdrop1, getdrop2, getdrop3, getdrop4, getdrop5;

    private static final String UPLOAD_URL = "https://project-alltest.com/appbutiful2/saverstaff_update2.php";
    private static final String UPLOAD_URL2 = "https://project-alltest.com/appbutiful2/upload_img.php"; // อัพโหลดรูป


    private EditText no1, no2, no3, no4, no5, no6, no7, no8, no9, no10, no11;
    RadioGroup Rao1;
    String Str0,Str1,Str2,Str3,Str4,Str5,Str6,Str7,Str8,Str9,Str10,Str11,Str12,Str13;
    RadioButton selectedRadioButton;
    String nameimg = "";
    String path = "";
    ImageView btnback;
    String Getstatus = "";
    Button btnstatus1, btnstatus2;
    String readdata1 = "https://project-alltest.com/appbutiful2/readdata1.php"; /// โหลดชื่อผู้ใช้

    ImageView btnimg1 , btnimg2, btnimg3, btnimg4, btnimg5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_profile);



        Bundle b = getIntent().getExtras();
        name = b.getString("A");

        bug2_image = (ImageView)findViewById(R.id.imageView2);
        tvPath    = (TextView)findViewById(R.id.bug2_path);
        bug2_image.setOnClickListener(this);

        Calendar c2 = Calendar.getInstance();
        SimpleDateFormat df2 = new SimpleDateFormat("HH:mm:ss");
        formattedDate2 = df2.format(c2.getTime());

        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        buttonsave = (Button) findViewById(R.id.save);
        buttonsave.setOnClickListener(this);

        buttoncancel = (Button) findViewById(R.id.cancel);
        buttoncancel.setOnClickListener(this);


        no1 = (EditText) findViewById(R.id.no1);
        no7 = (EditText) findViewById(R.id.no7);
        no8 = (EditText) findViewById(R.id.no8);

        no10 = (EditText) findViewById(R.id.no10);
        no11 = (EditText) findViewById(R.id.no11);

        btnback = (ImageView) findViewById(R.id.imgbtnback);
        btnback.setOnClickListener(this);



        String urlSuffix = "?txtKeyword="+name;
        class RegisterUser extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(member_profile.this, "Please Wait",null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                int A = s.length();

                String strJSON = s;
                try {
                    JSONArray data = new JSONArray(strJSON);

                    final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
                    HashMap<String, String> map;

                    for(int i = 0; i < data.length(); i++){
                        JSONObject c = data.getJSONObject(i);
                        int Row = i+1;
                        map = new HashMap<String, String>();
                        d1 =  c.getString("name");
                        d2 =  c.getString("address");
                        d3 =  c.getString("telphone");
                        d4 =  c.getString("username");
                        d5 =  c.getString("password");
                        d6 =  c.getString("img");
                        d7 =  c.getString("status");

                        MyArrList.add(map);

                    }


                    no1.setText(""+d1);
                    no7.setText(""+d3);
                    no8.setText(""+d2);

                    no10.setText(""+d4);
                    no11.setText(""+d5);

                    if(d6.equals("")){

                    }else{
                        String urlimg = "https://project-alltest.com/appbutiful2/img/"+d6;
                        Picasso.get().load(urlimg).into(bug2_image);
                    }



                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }



            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(readdata1+s);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String result;

                    result = bufferedReader.readLine();

                    return result;
                }catch(Exception e){
                    return null;
                }
            }
        }

        RegisterUser ru = new RegisterUser();
        ru.execute(urlSuffix);


        btnimg1 = (ImageView)findViewById(R.id.ic1);
        btnimg2 = (ImageView)findViewById(R.id.ic2);
        btnimg3= (ImageView)findViewById(R.id.ic3);

        btnimg1.setOnClickListener(this);
        btnimg2.setOnClickListener(this);
        btnimg3.setOnClickListener(this);
    }



    public void onClick(View view) {
        if(view == btnback) {

            Intent intent = new Intent(member_profile.this, member_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if (view == buttoncancel) {

            Intent intent = new Intent(member_profile.this, member_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else    if(view == btnimg1) {

            Intent intent = new Intent(member_profile.this, member_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == btnimg2) {

            Intent intent = new Intent(member_profile.this, member_cart.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if(view == btnimg3) {

            Intent intent = new Intent(member_profile.this, member_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if (view == bug2_image) {
            selectImage();

        } else if (view == buttonsave) {
            Str1 = no1.getText().toString().trim();

            Str7 = no7.getText().toString().trim();
            Str8 = no8.getText().toString().trim();

            Str10 = no10.getText().toString().trim();
            Str11 = no11.getText().toString().trim();

            Str12 = Getstatus;

            final Random numRandom = new Random();
            String code = String.valueOf(numRandom.nextInt(1000));
            nameimg = code+"-reimg"+"d2123";
            String Check_pic = tvPath.getText().toString().trim();


            if (Str1.equals("")) {

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_profile.this);
                builder.setCancelable(false);
                builder.setTitle("แจ้งเตือน");
                builder.setMessage(" กรุณากรอก ชื่อ - นามสกุล ");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();

            }else if (Str10.equals("")) {

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_profile.this);
                builder.setCancelable(false);
                builder.setTitle("แจ้งเตือน");
                builder.setMessage(" กรุณากรอก Username ");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();

            }else if (Str11.equals("")) {

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_profile.this);
                builder.setCancelable(false);
                builder.setTitle("แจ้งเตือน");
                builder.setMessage(" กรุณากรอก Password ");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();

            }else{

                if(Check_pic.equals("")){
                    nameimg = "";
                }else if(Check_pic.equals("null")) {
                    nameimg = "";
                }


                if(Check_pic.equals("")){

                }else if(Check_pic.equals("null")){

                }else{
                    if(image_camera.equals("1")){
                        path = path_camera;
                    }else{

                        path = getPath(filePath);
                    }

                    String urlSuffix = "?nameimg=";
                    class RegisterUser extends AsyncTask<String, Void, String> {

                        ProgressDialog loading;

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            loading = ProgressDialog.show(member_profile.this, "Please Wait",null, true, true);
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            loading.dismiss();
                        }

                        @Override
                        protected String doInBackground(String... params) {
                            int bytesRead, bytesAvailable, bufferSize;
                            byte[] buffer;
                            int maxBufferSize = 1 * 1024 * 1024;
                            int resCode = 0;
                            String resMessage = "";

                            String lineEnd = "\r\n";
                            String twoHyphens = "--";
                            String boundary =  "*****";

                            String strSDPath = path;
                            String strUrlServer = UPLOAD_URL2+"?nameimg="+nameimg;

                            try {
                                /** Check file on SD Card ***/
                                File file = new File(strSDPath);


                                FileInputStream fileInputStream = new FileInputStream(new File(strSDPath));

                                URL url = new URL(strUrlServer);
                                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                                conn.setDoInput(true);
                                conn.setDoOutput(true);
                                conn.setUseCaches(false);
                                conn.setRequestMethod("POST");

                                conn.setRequestProperty("Connection", "Keep-Alive");
                                conn.setRequestProperty("Content-Type",
                                        "multipart/form-data;boundary=" + boundary);

                                DataOutputStream outputStream = new DataOutputStream(conn
                                        .getOutputStream());
                                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                                outputStream.writeBytes("Content-Disposition: form-data; name=\"filUpload\";filename=\""
                                        + strSDPath + "\"" + lineEnd);
                                outputStream.writeBytes(lineEnd);

                                bytesAvailable = fileInputStream.available();
                                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                                buffer = new byte[bufferSize];

                                // Read file
                                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                                while (bytesRead > 0) {
                                    outputStream.write(buffer, 0, bufferSize);
                                    bytesAvailable = fileInputStream.available();
                                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                                }

                                outputStream.writeBytes(lineEnd);
                                outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                                // Response Code and  Message
                                resCode = conn.getResponseCode();
                                if(resCode == HttpURLConnection.HTTP_OK)
                                {
                                    InputStream is = conn.getInputStream();
                                    ByteArrayOutputStream bos = new ByteArrayOutputStream();

                                    int read = 0;
                                    while ((read = is.read()) != -1) {
                                        bos.write(read);
                                    }
                                    byte[] result = bos.toByteArray();
                                    bos.close();

                                    resMessage = new String(result);

                                }

                                Log.d("resCode=",Integer.toString(resCode));
                                Log.d("resMessage=",resMessage.toString());

                                fileInputStream.close();
                                outputStream.flush();
                                outputStream.close();


                            } catch (Exception ex) {
                                // Exception handling
                                return null;
                            }

                            return null;
                        }
                    }

                    RegisterUser ru = new RegisterUser();
                    ru.execute(urlSuffix);
                }


                String urlSuffix = "?no1="+Str1+"&no7="+Str7+"&no8="+Str8
                        +"&no10="+Str10+"&no11="+Str11+"&Getstatus="+Getstatus+"&nameimg="+nameimg+"&memberupdate="+name;

                class RegisterUser extends AsyncTask<String, Void, String> {

                    ProgressDialog loading;

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        loading = ProgressDialog.show(member_profile.this, "Please Wait",null, true, true);
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        loading.dismiss();
                        int A = s.length();
                        if(s.equals("บันทึกไม่สำเร็จ")){

                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_profile.this);
                            builder.setCancelable(false);
                            builder.setTitle("แจ้งเตือน");
                            builder.setMessage(" บันทึกไม่สำเร็จ ");
                            builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            builder.show();

                        }else if(s.equals("บันทึกเรียบร้อย")){

                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_profile.this);
                            builder.setCancelable(false);
                            builder.setTitle("แจ้งเตือน");
                            builder.setMessage(" บันทึกเรียบร้อย ");
                            builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();

                                    Intent intent = new Intent(member_profile.this, member_profile.class);
                                    intent.putExtra("A", name);
                                    startActivity(intent);
                                    finish();

                                }
                            });
                            builder.show();

                        }
                    }

                    @Override
                    protected String doInBackground(String... params) {
                        String s = params[0];
                        BufferedReader bufferedReader = null;
                        try {
                            URL url = new URL(UPLOAD_URL+s);
                            HttpURLConnection con = (HttpURLConnection) url.openConnection();
                            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                            String result;

                            result = bufferedReader.readLine();

                            return result;
                        }catch(Exception e){
                            return null;
                        }
                    }
                }

                RegisterUser ru = new RegisterUser();
                ru.execute(urlSuffix);


            }
        }
    }


    public void onBackPressed() {

    }


    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE ) {
            if(data != null) {
                Bundle extras = data.getExtras();

                Bitmap imageBitmap = (Bitmap) extras.get("data");
                bug2_image.setImageBitmap(imageBitmap);

                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);


                tvPath.setText("Path: " + getRealPathFromURI(tempUri));
                image_camera = "1";
                path_camera = getRealPathFromURI(tempUri);
            }
        }else if (requestCode == IMAGE_REQUEST_CODE )  {
            if(data != null) {
                filePath = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                    tvPath.setText("Path: ".concat(getPath(filePath)));
                    bug2_image.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    private void selectImage() {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(member_profile.this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {


                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }

                } else if (options[item].equals("Choose from Gallery")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Complete action using"), IMAGE_REQUEST_CODE);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }





}