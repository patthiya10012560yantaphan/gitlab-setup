package com.example.butiful2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.denzcoskun.imageslider.transformation.RoundedTransformation;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;


public class product_edit extends AppCompatActivity  implements   View.OnClickListener {

    String name, name2, name3 ;
    String name4;
    String name5;
    String d1 = "-";
    String d2 = "-";

    private static final int IMAGE_REQUEST_CODE = 3;
    private static final int STORAGE_PERMISSION_CODE = 123;
    private ImageView bug2_image ;
    private Bitmap bitmap;
    private Uri filePath;
    private TextView tvPath;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    String image_camera = "0";
    String path_camera = "";
    private Button buttonsave, buttoncancel;
    String formattedDate2;

    TextView bynuploadimg;

    private EditText no1, no2, no3, no4, no5, no6, no7, no8, no9, no10, no11;
    RadioGroup Rao1;
    String Str0,Str1,Str2,Str3,Str4,Str5,Str6,Str7,Str8,Str9,Str10,Str11,Str12,Str13;
    RadioButton selectedRadioButton;
    String nameimg = "";
    String path = "";
    ImageView btnback;
    private static final String UPLOAD_URL = "https://project-alltest.com/appbutiful2/saveproduct_update.php";
    private static final String UPLOAD_URL2 = "https://project-alltest.com/appbutiful2/upload_img.php"; // อัพโหลดรูป

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_edit);

        Bundle b = getIntent().getExtras();
        name = b.getString("D1");
        name2 = b.getString("D2");
        name3 = b.getString("D3");
        name4 = b.getString("D4");
        name5 = b.getString("A");


        bug2_image = (ImageView)findViewById(R.id.imageView2);
        tvPath    = (TextView)findViewById(R.id.bug2_path);
        bug2_image.setOnClickListener(this);

        Calendar c2 = Calendar.getInstance();
        SimpleDateFormat df2 = new SimpleDateFormat("HH:mm:ss");
        formattedDate2 = df2.format(c2.getTime());


        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        buttonsave = (Button) findViewById(R.id.save);
        buttonsave.setOnClickListener(this);

        buttoncancel = (Button) findViewById(R.id.cancel);
        buttoncancel.setOnClickListener(this);

        no1 = (EditText) findViewById(R.id.no1);
        no2 = (EditText) findViewById(R.id.no2);
        no1.setText(name2);
        no2.setText(name3);

        btnback = (ImageView) findViewById(R.id.imgbtnback);
        btnback.setOnClickListener(this);

        if(name4.equals("")){

        }else{
            String urlimg = "https://project-alltest.com/appbutiful2/img/"+name4;
            Picasso.get().load(urlimg).transform(new RoundedTransformation(50, 0)).into(bug2_image);

        }
    }



    public void onClick(View view) {
        if(view == btnback) {

            Intent intent = new Intent(product_edit.this, product.class);
            intent.putExtra("A", name5);
            startActivity(intent);
            finish();

        }else  if (view == buttoncancel) {

            Intent intent = new Intent(product_edit.this, product.class);
            intent.putExtra("A", name5);
            startActivity(intent);
            finish();

        }else  if (view == bug2_image) {
            selectImage();

        } else if (view == buttonsave) {
            Str1 = no1.getText().toString().trim();
            Str2 = no2.getText().toString().trim();


            final Random numRandom = new Random();
            String code = String.valueOf(numRandom.nextInt(1000));
            nameimg = code+"-"+"d2123";
            String Check_pic = tvPath.getText().toString().trim();



            if (Str1.equals("")) {

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(product_edit.this);
                builder.setCancelable(false);
                builder.setTitle("แจ้งเตือน");
                builder.setMessage(" กรุณากรอก ชื่อสินค้า ");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();

            }else if (Str2.equals("")) {

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(product_edit.this);
                builder.setCancelable(false);
                builder.setTitle("แจ้งเตือน");
                builder.setMessage(" กรุณากรอก ราคา ");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();

            }else{


                if(Check_pic.equals("")){
                    nameimg = "";
                }else if(Check_pic.equals("null")) {
                    nameimg = "";
                }


                if(Check_pic.equals("")){

                }else if(Check_pic.equals("null")){

                }else{
                    if(image_camera.equals("1")){
                        path = path_camera;
                    }else{

                        path = getPath(filePath);
                    }

                    String urlSuffix = "?nameimg=";
                    class RegisterUser extends AsyncTask<String, Void, String> {

                        ProgressDialog loading;

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            loading = ProgressDialog.show(product_edit.this, "Please Wait",null, true, true);
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            loading.dismiss();
                        }

                        @Override
                        protected String doInBackground(String... params) {
                            int bytesRead, bytesAvailable, bufferSize;
                            byte[] buffer;
                            int maxBufferSize = 1 * 1024 * 1024;
                            int resCode = 0;
                            String resMessage = "";

                            String lineEnd = "\r\n";
                            String twoHyphens = "--";
                            String boundary =  "*****";

                            String strSDPath = path;
                            String strUrlServer = UPLOAD_URL2+"?nameimg="+nameimg;

                            try {
                                /** Check file on SD Card ***/
                                File file = new File(strSDPath);


                                FileInputStream fileInputStream = new FileInputStream(new File(strSDPath));

                                URL url = new URL(strUrlServer);
                                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                                conn.setDoInput(true);
                                conn.setDoOutput(true);
                                conn.setUseCaches(false);
                                conn.setRequestMethod("POST");

                                conn.setRequestProperty("Connection", "Keep-Alive");
                                conn.setRequestProperty("Content-Type",
                                        "multipart/form-data;boundary=" + boundary);

                                DataOutputStream outputStream = new DataOutputStream(conn
                                        .getOutputStream());
                                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                                outputStream.writeBytes("Content-Disposition: form-data; name=\"filUpload\";filename=\""
                                        + strSDPath + "\"" + lineEnd);
                                outputStream.writeBytes(lineEnd);

                                bytesAvailable = fileInputStream.available();
                                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                                buffer = new byte[bufferSize];

                                // Read file
                                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                                while (bytesRead > 0) {
                                    outputStream.write(buffer, 0, bufferSize);
                                    bytesAvailable = fileInputStream.available();
                                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                                }

                                outputStream.writeBytes(lineEnd);
                                outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                                // Response Code and  Message
                                resCode = conn.getResponseCode();
                                if(resCode == HttpURLConnection.HTTP_OK)
                                {
                                    InputStream is = conn.getInputStream();
                                    ByteArrayOutputStream bos = new ByteArrayOutputStream();

                                    int read = 0;
                                    while ((read = is.read()) != -1) {
                                        bos.write(read);
                                    }
                                    byte[] result = bos.toByteArray();
                                    bos.close();

                                    resMessage = new String(result);

                                }

                                Log.d("resCode=",Integer.toString(resCode));
                                Log.d("resMessage=",resMessage.toString());

                                fileInputStream.close();
                                outputStream.flush();
                                outputStream.close();


                            } catch (Exception ex) {
                                // Exception handling
                                return null;
                            }

                            return null;
                        }
                    }

                    RegisterUser ru = new RegisterUser();
                    ru.execute(urlSuffix);
                }


                String urlSuffix = "?no1="+Str1+ "&no2="+Str2+"&nameimg="+nameimg+"&memberupdate="+name;

                class RegisterUser extends AsyncTask<String, Void, String> {

                    ProgressDialog loading;

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        loading = ProgressDialog.show(product_edit.this, "Please Wait",null, true, true);
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        loading.dismiss();
                        int A = s.length();
                        if(s.equals("บันทึกไม่สำเร็จ")){

                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(product_edit.this);
                            builder.setCancelable(false);
                            builder.setTitle("แจ้งเตือน");
                            builder.setMessage(" บันทึกไม่สำเร็จ ");
                            builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            builder.show();

                        }else if(s.equals("บันทึกเรียบร้อย")){

                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(product_edit.this);
                            builder.setCancelable(false);
                            builder.setTitle("แจ้งเตือน");
                            builder.setMessage(" บันทึกเรียบร้อย ");
                            builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();

                                    Intent intent = new Intent(product_edit.this, product.class);
                                    intent.putExtra("A", name5);
                                    startActivity(intent);
                                    finish();

                                }
                            });
                            builder.show();


                        }
                    }

                    @Override
                    protected String doInBackground(String... params) {
                        String s = params[0];
                        BufferedReader bufferedReader = null;
                        try {
                            URL url = new URL(UPLOAD_URL+s);
                            HttpURLConnection con = (HttpURLConnection) url.openConnection();
                            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                            String result;

                            result = bufferedReader.readLine();

                            return result;
                        }catch(Exception e){
                            return null;
                        }
                    }
                }

                RegisterUser ru = new RegisterUser();
                ru.execute(urlSuffix);

            }
        }
    }

    public void onBackPressed() {

    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE ) {
            if(data != null) {
                Bundle extras = data.getExtras();

                Bitmap imageBitmap = (Bitmap) extras.get("data");
                bug2_image.setImageBitmap(imageBitmap);

                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);


                tvPath.setText("Path: " + getRealPathFromURI(tempUri));
                image_camera = "1";
                path_camera = getRealPathFromURI(tempUri);
            }
        }else if (requestCode == IMAGE_REQUEST_CODE )  {
            if(data != null) {
                filePath = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                    tvPath.setText("Path: ".concat(getPath(filePath)));
                    bug2_image.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    private void selectImage() {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(product_edit.this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {


                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }

                } else if (options[item].equals("Choose from Gallery")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Complete action using"), IMAGE_REQUEST_CODE);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }





}