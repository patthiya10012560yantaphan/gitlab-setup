package com.example.butiful2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class qdata_view extends AppCompatActivity implements   View.OnClickListener {

    private Context context;
    String name, name2, name3;
    String datag1, datag2, datag3, datag4;
    String datag5;
    String d1 = "";
    String d2 = "";
    private TextView bm1, bm2, bm3, bm4, bm5, bm6;
    private EditText no1, no2, no3, no4, no5, no6, no7, no8;
    String Str1, Str2, Str3, Str4, Str5, Str6, Str7, nameimg, Str8;
    private static final String readdata3 = "https://project-alltest.com/appbutiful2/load_cart2.php"; /// สินค้า

    private Button btnConfrim;
    private ImageView btnBack;
    private Button btnView;
    ImageView btnimg1, btnimg2, btnimg3, btnimg4, btnimg5;
    int sumall = 0;
    int sumalltotalpoint = 0;
    int cart_count = 0;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private int date, month, yeat;
    String formattedDate2, formattedDate3;
    String Loaddata1 = "https://project-alltest.com/appbutiful2/loaddrop1_m_status.php";
    String getdrop1, getdrop2, getdrop3, getdrop4, getdrop5;
    private static final String UPLOAD_URL = "https://project-alltest.com/appbutiful2/saveupdatestatus.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qdata_view);


        Bundle b = getIntent().getExtras();
        name = b.getString("A");

        name2 = b.getString("B");

        datag1 = b.getString("D1");
        datag2 = b.getString("D2");
        datag3 = b.getString("D3");
        datag4 = b.getString("D4");

        datag5 = b.getString("D5");

        String urlSuffix2 = "?txtKeyword=" + datag5;
        class RegisterUser2 extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(qdata_view.this, "Please Wait", null, true, true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                int A = s.length();
                readdataliststatus(s);
            }


            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(Loaddata1 + s);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String result;

                    result = bufferedReader.readLine();

                    return result;
                } catch (Exception e) {
                    return null;
                }
            }
        }

        RegisterUser2 ru2 = new RegisterUser2();
        ru2.execute(urlSuffix2);

        btnimg1 = (ImageView) findViewById(R.id.ic1);
        btnimg2 = (ImageView) findViewById(R.id.ic2);
        btnimg3 = (ImageView) findViewById(R.id.ic3);

        btnimg1.setOnClickListener(this);
        btnimg2.setOnClickListener(this);
        btnimg3.setOnClickListener(this);

        bm2 = (TextView) findViewById(R.id.titledata);
        bm2.setText(" เลขที่ทำการจอง : " + name2);


        bm1 = (TextView) findViewById(R.id.totalprice);
        String urlSuffix3 = "?txtKeyword=" + name2;
        class RegisterUser3 extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(qdata_view.this, "Please Wait", null, true, true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                int A = s.length();

                readdatalist(s);

            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(readdata3 + s);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String result;

                    result = bufferedReader.readLine();

                    return result;
                } catch (Exception e) {
                    return null;
                }
            }
        }

        RegisterUser3 ru3 = new RegisterUser3();
        ru3.execute(urlSuffix3);

        loadpricetotal();


        no1 = (EditText) findViewById(R.id.e1);
        no2 = (EditText) findViewById(R.id.e2);
        no3 = (EditText) findViewById(R.id.e3);

        no1.setText(datag1);
        no2.setText(datag2);
        no3.setText(datag3);

        Calendar c2 = Calendar.getInstance();
        SimpleDateFormat df2 = new SimpleDateFormat("HH:mm");
        formattedDate2 = df2.format(c2.getTime());
        SimpleDateFormat df3 = new SimpleDateFormat("dd-MM-yyyy");
        formattedDate3 = df3.format(c2.getTime());
        date = c2.get(Calendar.DAY_OF_MONTH);
        month = c2.get(Calendar.MONTH);
        yeat = c2.get(Calendar.YEAR);


        btnBack = (ImageView) findViewById(R.id.imgbtnback);
        btnBack.setOnClickListener(this);

        int sumall = Integer.parseInt(datag4);
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String yourFormattedString = formatter.format(sumall);
        bm1.setText(" ยอดเงินชำระ : " + yourFormattedString + " บาท ");

        btnConfrim = (Button) findViewById(R.id.savedata);
        btnConfrim.setOnClickListener(this);


    }


    private void readdataliststatus(String s) {
        String strJSON = s;
        try {
            JSONArray data = new JSONArray(strJSON);

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);
                int Row = i + 1;
                map = new HashMap<String, String>();
                map.put("data", c.getString("data"));
                map.put("name", c.getString("name"));

                MyArrList.add(map);

            }

            // Adapter ตัวแรก
            SimpleAdapter sAdap;
            sAdap = new SimpleAdapter(qdata_view.this, MyArrList, R.layout.activity_spinner,
                    new String[]{"data", "name"}, new int[]{R.id.ColGalleryID, R.id.ColPatn});

            final Spinner drop1 = (Spinner) findViewById(R.id.spinner4);
            drop1.setAdapter(sAdap);
            // setOnItemSelectedListener
            drop1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    String sMemberID = MyArrList.get(position).get("data").toString();
                    getdrop1 = sMemberID;


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void loadpricetotal() {
        String urlSuffix3 = "?txtKeyword=" + name;
        class RegisterUser3 extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(qdata_view.this, "Please Wait", null, true, true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                int A = s.length();

                readdatalist2(s);

            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(readdata3 + s);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String result;

                    result = bufferedReader.readLine();

                    return result;
                } catch (Exception e) {
                    return null;
                }
            }
        }

        RegisterUser3 ru3 = new RegisterUser3();
        ru3.execute(urlSuffix3);
    }

    private void readdatalist(String s) {
        sumall = 0;
        sumalltotalpoint = 0;

        String strJSON = s;
        try {
            JSONArray data = new JSONArray(strJSON);

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);
                int Row = i + 1;
                map = new HashMap<String, String>();
                map.put("count", Integer.toString(Row));
                map.put("id", c.getString("id"));
                map.put("name", c.getString("name"));
                map.put("price", c.getString("price"));
                map.put("price2", c.getString("price2"));
                map.put("img", c.getString("img"));
                map.put("total", c.getString("total"));
                map.put("bill_no", c.getString("bill_no"));
                map.put("date_start", c.getString("date_start"));
                map.put("date_time", c.getString("date_time"));
                map.put("status_payment", c.getString("status_payment"));


                String totalpro = c.getString("total");
                String priceall = c.getString("price");
                sumall += (Integer.parseInt(totalpro) * Integer.parseInt(priceall));
                sumalltotalpoint += (Integer.parseInt(totalpro));

                MyArrList.add(map);

            }


            final ListView listViewMovies = (ListView) findViewById(R.id.list1);
            listViewMovies.setAdapter(new ImageAdapter(this, MyArrList));

            final androidx.appcompat.app.AlertDialog.Builder viewDetail = new androidx.appcompat.app.AlertDialog.Builder(this);
            final LayoutInflater factory = LayoutInflater.from(this);
            // OnClick Item
            listViewMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView,
                                        int position, long mylng) {


                }
            });


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void readdatalist2(String s) {
        sumall = 0;
        sumalltotalpoint = 0;
        String strJSON = s;
        try {
            JSONArray data = new JSONArray(strJSON);

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);
                int Row = i + 1;
                map = new HashMap<String, String>();
                map.put("count", Integer.toString(Row));
                map.put("id", c.getString("id"));
                map.put("name", c.getString("name"));
                map.put("price", c.getString("price"));
                map.put("price2", c.getString("price2"));
                map.put("img", c.getString("img"));
                map.put("total", c.getString("total"));
                map.put("bill_no", c.getString("bill_no"));
                map.put("date_start", c.getString("date_start"));
                map.put("date_time", c.getString("date_time"));
                map.put("status_payment", c.getString("status_payment"));


                String totalpro = c.getString("total");
                String priceall = c.getString("price");
                sumall += (Integer.parseInt(totalpro) * Integer.parseInt(priceall));
                sumalltotalpoint += (Integer.parseInt(totalpro));


                MyArrList.add(map);

            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public class ImageAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter(Context c, ArrayList<HashMap<String, String>> list) {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.activity_column_cart2, null);
            }


            ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView2);
            String chk1 = MyArr.get(position).get("img");
            String url = "";
            if (!MyArr.get(position).get("img").equals("")) {

                url = "https://project-alltest.com/appbutiful2/img/" + MyArr.get(position).get("img");
                Picasso.get().load(url).into(imageView);

            } else {

            }

            // ColPosition
            TextView txtPosition1 = (TextView) convertView.findViewById(R.id.name_order);
            txtPosition1.setText(Html.fromHtml(MyArr.get(position).get("name")));

            TextView txtPosition2 = (TextView) convertView.findViewById(R.id.name_order2);
            txtPosition2.setText(Html.fromHtml(" ราคา  " + MyArr.get(position).get("price2") + " บาท "));

            final TextView quantityText = (TextView) convertView.findViewById(R.id.quantity);
            quantityText.setText(" จำนวน  " + MyArr.get(position).get("total").toString() + " รายการ ");


            return convertView;

        }


    }


    public void onClick(View view) {
        if (view == btnimg1) {

            Intent intent = new Intent(qdata_view.this, admin_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        } else if (view == btnimg2) {

            Intent intent = new Intent(qdata_view.this, product.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        } else if (view == btnimg3) {

            Intent intent = new Intent(qdata_view.this, qdata.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        } else if (view == btnBack) {

            Intent intent = new Intent(qdata_view.this, admin_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        } else if (view == btnConfrim) {

            Str1 = getdrop1;

            String urlSuffix = "?no1=" + Str1 + "&bill=" + name2;

            class RegisterUser extends AsyncTask<String, Void, String> {

                ProgressDialog loading;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    loading = ProgressDialog.show(qdata_view.this, "Please Wait", null, true, true);
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    loading.dismiss();
                    int A = s.length();
                    if (s.equals("บันทึกไม่สำเร็จ")) {

                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(qdata_view.this);
                        builder.setCancelable(false);
                        builder.setTitle("แจ้งเตือน");
                        builder.setMessage(" บันทึกไม่สำเร็จ ");
                        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        builder.show();

                    } else if (s.equals("บันทึกเรียบร้อย")) {

                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(qdata_view.this);
                        builder.setCancelable(false);
                        builder.setTitle("แจ้งเตือน");
                        builder.setMessage(" บันทึกเรียบร้อย ");
                        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();

                                Intent intent = new Intent(qdata_view.this, admin_home.class);
                                intent.putExtra("A", name);
                                startActivity(intent);
                                finish();

                            }
                        });
                        builder.show();

                    }
                }

                @Override
                protected String doInBackground(String... params) {
                    String s = params[0];
                    BufferedReader bufferedReader = null;
                    try {
                        URL url = new URL(UPLOAD_URL + s);
                        HttpURLConnection con = (HttpURLConnection) url.openConnection();
                        bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                        String result;

                        result = bufferedReader.readLine();

                        return result;
                    } catch (Exception e) {
                        return null;
                    }
                }
            }

            RegisterUser ru = new RegisterUser();
            ru.execute(urlSuffix);

        }
    }


    public void onBackPressed() {

    }


}