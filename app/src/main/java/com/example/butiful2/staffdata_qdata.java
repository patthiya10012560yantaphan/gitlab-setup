package com.example.butiful2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class staffdata_qdata extends AppCompatActivity  implements   View.OnClickListener {

    private Context context;
    String name, name2, name3;
    String d1 = "";
    String d2 = "";
    private TextView bm1, bm2, bm3, bm4, bm5, bm6 ;
    private EditText no1, no2, no3, no4, no5, no6, no7, no8;
    String Str1,Str2,Str3,Str4,Str5,Str6,Str7,nameimg,Str8;
    private Button btnConfrim;
    private Button btnBack;
    private Button btnView;
    private Button btnall;
    ImageView btnimg1 , btnimg2, btnimg3, btnimg4, btnimg5;
    int sumall = 0;
    int sumalltotalpoint = 0;
    int cart_count=0;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private int date, month, yeat;
    String formattedDate2, formattedDate3;
    private static final String readdata3 = "https://project-alltest.com/appbutiful2/loadbill_member_admin_qdata2.php";
    private static final String readdata4 = "https://project-alltest.com/appbutiful2/loadbill_member_admin2_qdata2.php";

    ImageView btnback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staffdata_qdata);

        Bundle b = getIntent().getExtras();
        name = b.getString("A");


        btnimg1 = (ImageView)findViewById(R.id.ic1);
        btnimg2 = (ImageView)findViewById(R.id.ic2);
        btnimg3= (ImageView)findViewById(R.id.ic3);

        btnimg1.setOnClickListener(this);
        btnimg2.setOnClickListener(this);
        btnimg3.setOnClickListener(this);



        String urlSuffix3 = "?member_user="+name;
        class RegisterUser3 extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(staffdata_qdata.this, "Please Wait", null, true, true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                int A = s.length();

                readdatalist(s);

            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(readdata3 + s);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String result;

                    result = bufferedReader.readLine();

                    return result;
                } catch (Exception e) {
                    return null;
                }
            }
        }

        RegisterUser3 ru3 = new RegisterUser3();
        ru3.execute(urlSuffix3);

        no1 = (EditText) findViewById(R.id.e1);
        no1.setOnClickListener(this);


        btnall = (Button) findViewById(R.id.btnall);
        btnall.setOnClickListener(this);

        btnback = (ImageView) findViewById(R.id.imgbtnback);
        btnback.setOnClickListener(this);

    }


    private void readdatalist(String s) {
        String strJSON = s;
        try {
            JSONArray data = new JSONArray(strJSON);

            final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;

            for (int i = 0; i < data.length(); i++) {
                JSONObject c = data.getJSONObject(i);
                int Row = i + 1;
                map = new HashMap<String, String>();
                map.put("count", Integer.toString(Row));
                map.put("id", c.getString("id"));
                map.put("name", c.getString("name"));
                map.put("img", c.getString("img"));
                map.put("price", c.getString("price"));
                map.put("price2", c.getString("price2"));
                map.put("total", c.getString("total"));
                map.put("date_start", c.getString("date_start"));
                map.put("date_time", c.getString("date_time"));
                map.put("status_paymet", c.getString("status_paymet"));
                map.put("member_date", c.getString("member_date"));
                map.put("member_time", c.getString("member_time"));
                map.put("note", c.getString("note"));
                map.put("bill_no", c.getString("bill_no"));

                MyArrList.add(map);

            }


            final ListView listViewMovies = (ListView) findViewById(R.id.list1);
            listViewMovies.setAdapter(new ImageAdapter(this, MyArrList));

            final androidx.appcompat.app.AlertDialog.Builder viewDetail = new androidx.appcompat.app.AlertDialog.Builder(this);
            final LayoutInflater factory = LayoutInflater.from(this);
            // OnClick Item
            listViewMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView,
                                        int position, long mylng) {


                }
            });

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public class ImageAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<HashMap<String, String>> MyArr = new ArrayList<HashMap<String, String>>();

        public ImageAdapter(Context c, ArrayList<HashMap<String, String>> list) {
            // TODO Auto-generated method stub
            context = c;
            MyArr = list;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArr.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.activity_column_member_history, null);
            }

            String status_payment2 = " รอการตรวจสอบ   ";
            if(MyArr.get(position).get("status_paymet").equals("0")) {
                status_payment2 = " <font color = '#FFA000'>  รอการตรวจสอบ    </font>";

            }else  if(MyArr.get(position).get("status_paymet").equals("1")) {
                status_payment2 = " <font color = '#006400'> อนุมัติ </font>";

            }else  if(MyArr.get(position).get("status_paymet").equals("2")) {
                status_payment2 = " <font color = 'red'> ไม่อนุมัติ </font>";

            }

            // ColPosition
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            int sumall =  Integer.parseInt(MyArr.get(position).get("price2"));
            int sumall2 =  Integer.parseInt(MyArr.get(position).get("price"));

            String yourFormattedString1 = formatter.format(sumall);
            String yourFormattedString2 = formatter.format(sumall2);

            TextView txtPosition1 = (TextView) convertView.findViewById(R.id.name_order);
            txtPosition1.setText(Html.fromHtml(
                    " เลขที่ทำรายการ " + MyArr.get(position).get("bill_no") + " <br> "+
                            " วันที่ทำรายการ " +
                            dateThai(MyArr.get(position).get("date_start")) +
                            " เวลา  " + (MyArr.get(position).get("date_time")) + " <br> "+
                            " วันที่เข้าใช้บริการ " +
                            dateThai(MyArr.get(position).get("member_date")) +
                            " <br> "+ " คิวที่  " + (MyArr.get(position).get("count"))
            ));

            // ColPosition
            TextView btnview2 = (TextView) convertView.findViewById(R.id.btnview2);
            btnview2.setText(Html.fromHtml(" สถานะ " + status_payment2 + "  " + " ยอดชำระ  " + sumall + " บาท "
            ));

            // ColPosition
            ImageView btndelete = (ImageView) convertView.findViewById(R.id.btndelete);
            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final String id_order = MyArr.get(position).get("bill_no");
                    final String data1 = MyArr.get(position).get("member_date");
                    final String data2 = MyArr.get(position).get("member_time");
                    final String data3 = MyArr.get(position).get("note");
                    final String data4 = MyArr.get(position).get("price2");
                    final String data5 = MyArr.get(position).get("status_paymet");

                    Intent intent = new Intent(staffdata_qdata.this, staffdata_qdata_view.class);
                    intent.putExtra("A", name);
                    intent.putExtra("B", id_order);

                    intent.putExtra("D1", data1);
                    intent.putExtra("D2", data2);
                    intent.putExtra("D3", data3);
                    intent.putExtra("D4", data4);

                    intent.putExtra("D5", data5);
                    startActivity(intent);
                    finish();

                }
            });
            return convertView;

        }


    }

    public void onClick(View view) {
        if(view == btnback) {

            Intent intent = new Intent(staffdata_qdata.this, staffdata_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if(view == btnimg1) {

            Intent intent = new Intent(staffdata_qdata.this, staffdata_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else   if(view == btnimg2) {

            Intent intent = new Intent(staffdata_qdata.this, staffdata_product.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if(view == btnimg3) {

            Intent intent = new Intent(staffdata_qdata.this, staffdata_qdata.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if(view == btnBack) {

            Intent intent = new Intent(staffdata_qdata.this, staffdata_home.class);
            intent.putExtra("A", name);
            startActivity(intent);
            finish();

        }else  if(view == btnall) {

            String senddate = no1.getText().toString();
            String urlSuffix3 = "?member_user="+name+"&date="+senddate;
            class RegisterUser3 extends AsyncTask<String, Void, String> {

                ProgressDialog loading;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    loading = ProgressDialog.show(staffdata_qdata.this, "Please Wait", null, true, true);

                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    loading.dismiss();
                    int A = s.length();

                    readdatalist(s);

                }

                @Override
                protected String doInBackground(String... params) {
                    String s = params[0];
                    BufferedReader bufferedReader = null;
                    try {
                        URL url = new URL(readdata4 + s);
                        HttpURLConnection con = (HttpURLConnection) url.openConnection();
                        bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                        String result;

                        result = bufferedReader.readLine();

                        return result;
                    } catch (Exception e) {
                        return null;
                    }
                }
            }

            RegisterUser3 ru3 = new RegisterUser3();
            ru3.execute(urlSuffix3);



        }else if(view == no1) {

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    no1.setText(i2 + "-" + (i1 + 1) + "-" + i);
                    String senddate = i2 + "-" + (i1 + 1) + "-" + i;
                }
            }  , yeat, month, date);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        }
    }

    public void onBackPressed() {

    }

    public static String dateThai(String strDate)
    {
        String Months[] = {
                "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน",
                "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม",
                "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

        int year=0,month=0,day=0;
        try {
            Date date = df.parse(strDate);
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DATE);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return String.format("%s %s %s", day,Months[month],year+543);
    }


}